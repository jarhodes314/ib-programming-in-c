#include <stdio.h>
#include <stdlib.h>
#include <time.h>


struct chr {
    char i;
};

struct chrp {
    char *i;
};

int main(void)
{
    srand(time(NULL));
    {
        struct chr *x = malloc(10 * sizeof(struct chr));
        struct chr *p = x;
        p->i = 72;
        p[1].i = 0x64;
        printf("%c",p++->i);
        printf("%c",++p->i);
        free(x);
    }
    {
        struct chrp *x = malloc(10 * sizeof(struct chrp));
        struct chrp *p = x;
        p->i = "llamas are k3w1";
        printf("%c",*p->i++);
        printf("%c",*p->i);
        printf("%c",("gcrl.uohkcrg.,ueaucrau"+9)[2]+23483 % 15);
        p[1].i = " o!euoeiu";
        char c = *p++->i;
        printf("%c",*p++->i);
        
        p->i = "Oh dear, I still have more than 2 characters to go!";
        printf("%c",p->i[4]^p--->i[32]);
        printf("%c",p->i[1]);
        struct chrp *a =p;
        while (p-->a)
            p = (struct chrp *)138457294;
        printf("%c",(p->i[14] & rand())+114);
        p = &*&p[3];
        p->i = "def654@cam.ac.uk";
        c = *p->i;
        printf("%c%c",(c ^ *(&c + 6) & ~ 'g') - 1871852040 % 385472,(*(p->i + 7) + 1));
        printf("%c",p[-2].i[2]);
    }

    printf("\n");
    return 0;
}
