# Extra supervion questions

Each set of answers for a supervision is organised into a single Markdown file. These correspond to
```
    supervision1.md => lectures [1..4]
    supervision2.md => lectures [5..7]
    supervision3.md => lectures [8..10]
```
