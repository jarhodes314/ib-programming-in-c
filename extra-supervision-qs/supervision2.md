# Supervision 2 Questions
## Question 1
- `i = sizeof(char);`
`char` is a single-byte in C, so `i == 1`

- `i = sizeof(int);`
This will set `i` to be the size of an `int` which can depend on the architecture compiled for and the compiler. `i <= sizeof(long) && i >= sizeof(short)` will evaluate to true because that is how `int` is defined in the C specification.

- `int a; i = sizeof a;`
Same as previous question.

- `char b[5]; i = sizeof(b);`
`i` is 5 since `sizeof(b) == sizeof(char) * 5 == 1 * 5`

- `char *c = b; i = sizeof(c);`
`c` is a pointer to a character, so `i` is the size of a pointer in bytes, which depends on the architecture.

- `struct { int d; char e;} s; i = sizeof(s);`
`i == sizeof(int) + sizeof(char)`

- `void f(int j[5]) { i = sizeof(j); }`
`i == sizeof(int) * 5`

- `void f(int j[][10]) { i = sizeof(j); }`

## Question 2
`long` is the largest type of the three. It must be at least 4 bytes, so N is at most `34 / 4 = 8`

## Question 3
1. Yes, because `MAX_INT` is undefined. If we set `N` to be `INT_MAX`, this will probably segfault (unless the OS has some other way of dealing with itor we attempt to run it on an Arduino, for example, in which case it is probably undefined behaviour).

2. This will work just fine, the compiler might optimise the loops away in fact, since they will never satisfy the condition to enter them.

3. Setting N too high we result in accessing unallocated memory, and therefore probably a segmentation fault. If N is less than 1, the program won't print anything.

## Question 4
Since the function uses integer division, if `b` is `0`, the behaviour of the function is undefined. Since both arguments to `/` are of type `int32_t`, the result is of `int32_t` and will probably be truncated to an `int16_t` when returned. We can use UndefinedBehaviourSanitizer to establish whether this occurs during the execution of our program.

## Question 5
This function could cause a memory leak if we don't call `free(ptr)`. This will be caught by AddressSanitizer.

## Question 6
An arena allows us to store multiple objects in a large array to allow bulk allocations and de-allocations. This is useful because memory allocations are expensive, but doing 1 allocation of size N is much cheaper than N allocations of size 1. We can allocate an arena and then when we create an object such as a tree, we create a pointer to a space in the `elts` array which can be used just like a pointer allocated using `malloc`, except we will call `free` on the arena as a whole, rather than the individual elements.
- `size` stores the number of items the buffer can store. This allows us to establish when the buffer is full
- `current` points to the first empty space in the buffer
- `elts` is the array of elements in the arena

## Question 7
*It will print an error due to the missing semicolons during compilation.*
7
6

## Question 8
It will get `x` from `vec` and then return the address which is assigned to the variable `vector_point`.

## Question 9
The program won't compile since `z` is not declared in the scope of `main()`.

## Question 10
```c
#include <stdio.h>
#define MAGIC 9

int* bubbleSort(int* arr, int n)
{
    int swapsMade = -1;
    for (int i = 0; swapsMade & i < n; i++)
    {
        swapsMade = 0;
        for (int j = 0; j < n - i - 1; j++)
        {
            if(arr[j+1] < arr[j])
            {
                int tmp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = tmp;
                swapsMade = 1;
            }
        }
    }
}

int main(void)
{
    FILE *fin = fopen("in.txt","r");
    int arr[MAGIC];
    for(int i = 0; i < MAGIC; i++) fscanf(fin,"%d",arr[i]);
    fclose(fin);
    bubbleSort(arr,MAGIC);
    FILE *fout = fopen("out.txt","w");
    for(int i = 0; i < MAGIC; i++) fprintf(fout,"%d",arr[i]);
    fclose(fout);
    return 0;
}
```

## Question 11
```c
struct my_vec {
    int *array;
    // you can add more varibles that will help you to anser the question
};

typedef struct my_vec vec;
```
