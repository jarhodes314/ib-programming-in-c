# Exercises from Lectures 1-3
## Lecture 1
1. 'a' is a character literal. This means it can be stored as a single byte on the stack using ```char a = 'a'```. "a", on the other hand, is a string literal. This means that it could be stored using `char *b = "a"`. This will take up 2 bytes on the heap (binary `0110_0001 0000_0000`) as a null-terminator is required to mark the end of a C-string. It will also require ```sizeof(char *)``` space on the stack for the pointer (which will be 8 bytes on a 64-bit system).

2. The `,` operator is a binary infix operator that will evaluate both arguments, and return the result of the second. Since `i<10` has no side-effects, the result of evaluating it will be discarded and therefore the loop can be simplified to ```for(i=0; j!=5; i++,j++)```. This will terminate when j reaches the value of 5, which may happen via overflow. How likely this is to require overflow depends on how the program was compiled, since `char` is not defined to be signed or unsigned in the C specification. The value of `j` depends on the code before, since in C, variables are not reset to a default value at the declaration, and thus `j`'s value is whatever the value in memory was at the location at which `j` was placed.

3. 
```c
void swap_int(int arr[], int a, int b)
{
        arr[a] ^= arr[b];
        arr[b] ^= arr[a];
        arr[a] ^= arr[b];
}

int* sort_int(int arr[], int len)
{
        bool swapped = true;
        for (int a = 0; a < len && swapped; a++)
        {
                swapped = false;
                for (int b = a + 1; b < len; b++)
                {
                        if (arr[b] < arr[a]) swap_int(arr, a, b);
                        swapped = true;
                }
        }
        int i;
        for (i = 0; i < len - 1; i++)
                printf("%d, ", arr[i]);

        printf("%d\n", arr[i]);
        return arr;
}
```
4.
```c
void swap_char(char arr[], int a, int b)
{
        arr[a] ^= arr[b];
        arr[b] ^= arr[a];
        arr[a] ^= arr[b];
}

char* sort_char(char arr[], int len)
{
        bool swapped = true;
        for (int a = 0; a < len && swapped; a++)
        {
                swapped = false;
                for (int b = a + 1; b < len; b++)
                {
                        if (arr[b] < arr[a]) swap_char(arr, a, b);
                        swapped = true;
                }
        }

        int i;
        for (i = 0; i < len - 1; i++)
                printf("%c, ", arr[i]);

        printf("%c\n", arr[i]);
        return arr;
}
```

## Lecture 2

1.
```c
int cntlower(char str[], unsigned int len)
{
        int count = 0;
        for (int i = 0; i < len; i++)
        {
                if (isalpha(str[i]) && (str[i] & 0x60 == 0x60)) count++;
        }

        return count;
}
```

2.
```c
void merge(int arr[], int aLength, int bLength, int iA)
{
        int iB = iA + aLength;
        const int aLim = aLength + iA;
        const int bLim = bLength + iB;
        const int startPoint = iA;
        int space[aLength+bLength];
        int iSpace = 0;
        while (iA < aLim && iB < bLim)
                space[iSpace++] = arr[iA] <= arr[iB] ? arr[iA++] : arr[iB++];
        while (iA < aLim)
                space[iSpace++] = arr[iA++];
        while (iB < bLim)
                space[iSpace++] = arr[iB++]; 
        for (int i = 0; i < iSpace; i++)
                arr[i + startPoint] = space[i];
}

void _mergeSort(int arr[], int length, int iStart)
{
        if (length >= 2)
        {
                _mergeSort(arr, length / 2, iStart);
                _mergeSort(arr, length - length /2, iStart + length / 2);
                merge(arr,length / 2, length - length / 2,iStart);
        }
}

void mergeSort(int arr[], int length)
{
        _mergeSort(arr, length, 0);
}
```
3.
```c
#DEFINE SWAP(t,x,y) t tmp = x; x = y; y = tmp;
```
4. No, because it will increment i twice, once when it sets tmp and then a second when it sets v[i++] since macros in C are based on text replacement.

5.
```c
#DEFINE SWAP(x,y) x ^= y; y ^= x; x ^= y;
```

## Lecture 3
1. ```p[-2]``` means the value at the address of the pointer minus 2 * sizeof(*p) bytes. This would be legal if p pointed to a location in an array more than two elements in. e.g. 
```c
int q[10] = {0,1,2,3,4,5,6,7,8,9};
int *p = &q[7];
printf("%d", p[-2]); /* prints 5 */
```
2. 
```c
char *strfind(const char *s, const char *f)
{
        int iS = 0;
        int iF = 0;
        int start = -1;
        while(s[iS])
        {
                if (!f[iF]) return NULL;
                if (s[iS] == f[iF])
                {
                        start = start == -1 ? iF : start;
                        iS++;
                        iF++;
                }
                else
                {
                        if (start < 0) iF++;
                        start = -1;
                        iS = 0;
                }
        }
        return &f[start];
}
```
3.
    * ```++p->i``` first increments the pointer p, returning the updated pointer, dereferences it and gets the value of i from the struct the updated pointer points to.
    * ```p++->i``` is the same as above, except it returns i from the original value of the pointer
    * ```*p->i``` gets the item i from the struct p points to, and then dereferences the result (so the struct contains a pointer of some type)
    * ```*p->i++``` gets the value of the pointer from item i in the struct p points to, increments that pointer and returns the original value of the pointer to the * operator, which dereferences the resulting pointer
    * ```(*p->i)++``` dereferences the pointer from item i in the struct p points to, then increments the result
    * ```*p++->i``` increments the pointer p, returning the original value of p, from which i is found (by derefencing p and then getting the value from the resulting struct), then dereferences the result
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct chr {
    char i;
};

struct chrp {
    char *i;
};

int main(void)
{
    srand(time(NULL));
    {
        struct chr *x = malloc(10 * sizeof(struct chr));
        struct chr *p = x;
        p->i = 72;
        p[1].i = 0x64;
        printf("%c",p++->i);
        printf("%c",++p->i);
        free(x);
    }
    {
        struct chrp *x = malloc(10 * sizeof(struct chrp));
        struct chrp *p = x;
        p->i = "llamas are k3w1";
        printf("%c",*p->i++);
        printf("%c",*p->i);
        printf("%c",("gcrl.uohkcrg.,ueaucrau"+9)[2]+23483 % 15);
        p[1].i = " o!euoeiu";
        char c = *p++->i;
        printf("%c",*p++->i);
        
        p->i = "Oh dear, I still have more than 2 characters to go!";
        printf("%c",p->i[4]^p--->i[32]);
        printf("%c",p->i[1]);
        struct chrp *a =p;
        while (p-->a)
            p = (struct chrp *)138457294;
        printf("%c",(p->i[14] & rand())+114);
        p = &*&p[3];
        p->i = "def654@cam.ac.uk";
        c = *p->i;
        printf("%c%c",(c ^ *(&c + 6) & ~ 'g') - 1871852040 % 385472,(*(p->i + 7) + 1));
        printf("%c",p[-2].i[2]);
        free(x);
    }

    printf("\n");
    return 0;
}
```
4.
```c
#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>

int main(int argc, char **argv)
{
        int numStack[argc - 1];
        int iStack = 0;
        numStack[0] = 0;
        for (int i = 1; i < argc; i++)
        {
                //printf("%s ", argv[i]);
                if (!isdigit(argv[i][0]) && !isdigit(argv[i][1]))
                {
                        if (iStack < 2)
                        {
                                printf("Error, not enough numbers provided, current result is %d", numStack[0]);
                                return 1;
                        }
                        iStack--;
                        switch(argv[i][0])
                        {
                                case '+':
                                        numStack[iStack - 1] += numStack[iStack];
                                        break;
                                case '-':
                                        numStack[iStack - 1] -= numStack[iStack];
                                        break;
                                case '*':
                                        numStack[iStack - 1] *= numStack[iStack];
                                        break;
                                case '/':
                                        numStack[iStack - 1] /= numStack[iStack];
                                        break;
                                default:
                                        printf("Unrecognised operator: '%c'", argv[i][0]);
                                        break;
                        }
                }
                else
                {
                        numStack[iStack++] = atoi(argv[i]);
                }
        }
        if (iStack != 1)
        {
                printf("Error, not enough operators provided, current result is %d", numStack[0]);
                return 2;
        }
        else
        {
                printf("%d", numStack[0]);
        }
}
```
