# Programming in C - Supervision 3
## Question 1
1. `assert` is used to state that a boolean expression should always be true and cause an error if the assertion is not satisfied at runtime. Since `swap` takes `int` arguments, the variables `x` and `y` in the function will be copies of those passed to it in `main`, so the original variables are left untouched. This means `x==0` once `swap(x,y)` has returned so the assertion will trigger.
2. 
```c
void swap(int *x, int *y)
{
    int temp = *x
    *x = *y;
    *y = temp;
}
```
```c++
void swap(int &x, int &y)
{
    int temp = x;
    x = y;
    y = temp;
}
```

## Question 2
`x` is 8 at the end of `main`. `::` in C++ is the scope resolution operator, so `::x` means x in the global scope.

## Question 3
1. 
2. 
```c++
class FileManager {
private:
    FILE *fp;

public:
    FileManager(char *fileName)
    {
        fp = fopen(fileName);
    }
    ~FileManager()
    {
        fclose(fp);
    }
}
```
