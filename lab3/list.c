#include <stdio.h>
#include <stdlib.h>
#include "list.h"

List *cons(int head, List *tail) { 
    List *cell = malloc(sizeof(List));
    cell->head = head;
    cell->tail = tail;
    return cell;
}

/* Functions for you to implement */

int sum(List *list) {
    /*
     * Iterate over the list until we hit a NULL pointer,
     * adding each value to total
     */
    int total = 0;
    for (; list; list = list->tail) 
    {
        total += list->head;
    }
    return total;
}

void iterate(int (*f)(int), List *list) {
    /*
     * Iterate over the list, dereference f and then apply the resulting
     * function to mutate the current value
     */
    for (; list; list = list->tail) 
    {
        list->head = (*f)(list->head);
    }
}

void print_list(List *list) { 
    /*
     * Iterate through the list, printing the values
     * and then add a newline at the end
     */
    for (; list->tail; list = list->tail) 
    {
        printf("%i, ",list->head);
    }
    if (list) printf("%i\n",list->head);
    else printf("\n");
}

/**** CHALLENGE PROBLEMS ****/

List *merge(List *list1, List *list2) {
    /*
     * Check the lists have non-NULL pointers and compare their heads
     * Add the smallest one to the result, and keep repeating until one
     * or other of the lists is empty
     */
    if (!list1) return list2;
    if (!list2) return list1;
    List *resultHead;
    List *result;
    if (list1->head <= list2->head)
    {
        result = list1;
        list1 = list1->tail;
    }
    else
    {
        result = list2;
        list2 = list2->tail;
    }
    resultHead = result;

    for (; list1 && list2; result = result->tail)
    {
        if (list1->head <= list2->head)
        {
            result->tail = list1;
            list1 = list1->tail;
        }
        else
        {
            result->tail = list2;
            list2 = list2->tail;
        }
    }

    result->tail = list1 ? list1 : list2;
    return resultHead;  
}

void split(List *list, List **list1, List **list2) { 
    /*
     * Iterate through the list, allocating elements in turn to
     * list1 and then list2
     */
    if (!list)
    {
        list1 = NULL;
        list2 = NULL;
        return;
    }

    List *list1elem = list;
    List *list2elem = list->tail;

    *list1 = list1elem;
    if (!list2elem)
    {
        list2 = NULL;
        return;
    }

    *list2 = list2elem;

    for (list = list->tail->tail; list; list = list->tail)
    {
        list1elem->tail = list;
        list1elem = list1elem->tail;

        list = list->tail;
        if (!list) break;

        list2elem->tail = list;
        list2elem = list2elem->tail;
    }

    list1elem->tail = NULL;
    list2elem->tail = NULL;
}

/* You get the mergesort implementation ;//for free. But it won't
   work unless you implement merge() and split() first! */

List *mergesort(List *list) { 
    if (list == NULL || list->tail == NULL)
    { 
        return list;
    }
    else
    { 
        List *list1;
        List *list2;
        split(list, &list1, &list2);
        list1 = mergesort(list1);
        list2 = mergesort(list2);
        return merge(list1, list2);
    }
}
