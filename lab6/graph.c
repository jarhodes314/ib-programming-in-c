#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "graph.h"

Node *empty = NULL;

Node *node(int value, Node *left, Node *right) { 
    Node *r = malloc(sizeof(Node));
    r->marked = false;
    r->value = value;
    r->left = left;
    r->right = right;
    return r;
}


/* Basic Problems */

int size(Node *node) {  
    if (!node || node->marked)
        return 0;
    node->marked = true;
    return 1 + size(node->left) + size(node->right);
}


void unmark(Node *node) { 
    if (!node || !node->marked)
        return;
    node->marked = false;
    unmark(node->left);
    unmark(node->right);
}

bool path_from(Node *node1, Node *node2) {
    if (!node1 || !node2 || node1->marked)
        return false;
    if (node1 == node2)
        return true;
    node1->marked = true;
    return path_from(node1->left,node2) || path_from(node1->right,node2);
}

bool cyclic(Node *node) { 
    return path_from(node->left,node) || path_from(node->right,node);
} 


/* Challenge problems */

void get_nodes(Node *node, Node **dest) { 
    if (!node)
        return;
    int n = size(node);
    unmark(node);
    dest[0] = node;
    int addto = 1;
    int current = 0;
    for (;addto < n && current < addto; current++) 
    {
        printf("%d %d, ", current, addto);
        dest[current]->marked = true;
        if (dest[current]->left && !dest[current]->left->marked)
        {
            dest[addto++] = dest[current]->left;
        }
        if (dest[current]->right && !dest[current]->right->marked)
        {
            dest[addto++] = dest[current]->right;
        }
    }
}

void graph_free(Node *node) { 
    if (!node)
        return;
    int n = size(node);
    unmark(node);
    Node **buffer = malloc(n * sizeof(Node *));
    get_nodes(node,buffer);
    for (int i = 0; i < n; i++)
    {
        printf("%d",buffer[i]);
        free(buffer[i]);
    }
    free(buffer);
}


