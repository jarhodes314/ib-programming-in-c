#include <ctype.h>
#include <string.h>
#include "revwords.h"

void reverse_substring(char str[], int start, int end)
{ 
    /*
     *  This uses the XOR swap algorithm to swap the characters in-place
     *  See https://en.wikipedia.org/wiki/XOR_swap_algorithm for more details
     *  It is safe to use since end != start if the condition is satisfied
     */
    while (end > start)
    {
        str[start] ^= str[end];
        str[end] ^= str[start];
        str[start++] ^= str[end--];
    }
}

int find_next_start(char str[], int len, int i)
{
    /*
     *  First check to see if we're at the start of the string
     *  since this isn't preceeded by a non-letter
     */
    if (i == 0 && isalpha(str[i]))
    {
        return i;
    }

    /*
     *  Then iterate through the string, checking for a non-letter
     *  followed by a letter
     */
    for (;i + 1 < len;i++)
    {
        if (!isalpha(str[i]) && isalpha(str[i + 1]))
        {
            return i + 1;
        }
    }

    /* 
     *  Return -1 by default, i.e. if there are no more words in the
     *  string
     */
    return -1;
}

int find_next_end(char str[], int len, int i)
{
    /*
     *  Same as find_next_start, except it checks for the opposite
     *  and the default value is len
     */
    for (;i + 1 < len;i++)
    {
        if (isalpha(str[i]))
        {
            if (!isalpha(str[i + 1]))
            { 
                return i + 1;
            }
        }
    }

    return len;
}

void reverse_words(char s[])
{ 
    /*
     *  Iterate through the string, finding the start of the next word,
     *  then finding the of that word, then calling reverse on the word 
     */
    int len = strlen(s);
    int end;
    for (int i = 0; i < len; i++)
    {
        int start = find_next_start(s, len, i);
        if (!(start + 1)) break;
        end = find_next_end(s, len, i);
        reverse_substring(s, start, end - 1);
        i = end - 1;
    }
}
