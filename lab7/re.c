#include <stdlib.h>
#include <stdio.h>
#include "re.h"

arena_t create_arena(int size)
{
    arena_t a = malloc(sizeof(struct arena));
    a->res = malloc(sizeof(Regexp) * size);
    a->size = size;
    a->current = 0;
    return a;
}

void arena_free(arena_t a)
{ 
    free(a->res);
    free(a);
}

Regexp *re_alloc(arena_t a)
{
    if (a->current < a->size)
        return &(a->res[a->current++]);
    else
        return NULL;
}

Regexp *re_chr(arena_t a, char c)
{
    Regexp *chr = re_alloc(a);
    if (chr)
    {
        chr->type = CHR;
        union data data = {.chr = c};
        chr->data = data;
    }
    
    return chr;
}

Regexp *re_alt(arena_t a, Regexp *r1, Regexp *r2)
{
    Regexp *alt = re_alloc(a);
    if (alt)
    {
        alt->type = ALT;
        union data data = {.pair ={.fst = r1, .snd = r2}};
        alt->data = data;
    }
    return alt;
}

Regexp *re_seq(arena_t a, Regexp *r1, Regexp *r2)
{ 
    Regexp *seq = re_alloc(a);
    if (seq)
    {
        seq->type = SEQ;
        union data data = {.pair ={.fst = r1, .snd = r2}};
        seq->data = data;
    }
    return seq;
}

int re_match(Regexp *r, char *s, int i)
{ 
    if (r != NULL)
    {
        int a, b;
        switch (r->type)
        {
            case CHR: ;
                if (s[i] == r->data.chr) return i + 1;
                break;
            case SEQ: ;
                a = re_match(r->data.pair.fst, s, i);
                if (a > i) return re_match(r->data.pair.snd, s, a);
                break;
            case ALT: ;
                a = re_match(r->data.pair.fst, s, i);
                b = re_match(r->data.pair.snd, s, i);
                return a >= b ? a : b;
                break;
        }
    }
    return -1;
}

void re_print(Regexp *r)
{ 
    if (r != NULL)
    { 
        switch (r->type)
        {
            case CHR: 
                printf("%c", r->data.chr);
                break;
            case SEQ:
                if (r->data.pair.fst->type == ALT)
                { 
                    printf("(");
                    re_print(r->data.pair.fst);
                    printf(")");
                }
                else
                {
                    re_print(r->data.pair.fst);
                }
                if (r->data.pair.snd->type == ALT)
                { 
                    printf("(");
                    re_print(r->data.pair.snd);
                    printf(")");
                }
                else
                {
                    re_print(r->data.pair.snd);
                }
                break;
            case ALT:
                re_print(r->data.pair.fst);
                printf("+");
                re_print(r->data.pair.snd);
                break;
        }
    }
    else
    { 
        printf("NULL");
    }
}    




