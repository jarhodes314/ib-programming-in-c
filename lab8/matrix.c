#include "matrix.h"
#include <stdbool.h>

matrix_t matrix_create(int rows, int cols)
{ 
    double *elts = malloc(rows * cols * sizeof(double));
    matrix_t m = {rows, cols, elts};
    return m;
}

double matrix_get(matrix_t m, int r, int c)
{ 
    //assert(r < m.rows && c < m.cols);
    return m.elts[r * m.cols + c];
}

void matrix_set(matrix_t m, int r, int c, double d)
{ 
    //assert(r < m.rows && c < m.cols);
    m.elts[r * m.cols + c] = d;
}

void matrix_free(matrix_t m)
{ 
    free(m.elts);
}

matrix_t matrix_multiply(matrix_t m1, matrix_t m2)
{ 
    matrix_t m = matrix_create(m1.rows,m2.cols);
    for (int r1 = 0;r1 < m1.rows; r1++)
    {
        for (int c2 = 0; c2 < m2.cols; c2++)
        {
            double total = 0.0;
            for (int c = 0; c < m1.cols; c++)
            {
                total += matrix_get(m2, c, c2) * matrix_get(m1, r1, c);
            }
            matrix_set(m,r1,c2,total);
        }
    }
    return m;
}

matrix_t matrix_transpose(matrix_t m)
{ 
    matrix_t t = matrix_create(m.cols,m.rows);
    for (int r = 0; r < m.rows; r++)
    {
        for (int c = 0; c < m.cols; c++)
        {
            matrix_set(t,c,r,matrix_get(m,r,c));
        }
    }
    return t;
}

matrix_t matrix_multiply_transposed(matrix_t m1, matrix_t m2)
{ 
    //assert(m1.cols == m2.cols);
    if (m1.rows < m2.rows) return matrix_multiply_transposed(m2,m1);
    matrix_t m = matrix_create(m1.rows,m2.rows);
    for (int r1 = 0; r1 < m1.rows; r1++)
    {
        for (int r2 = 0; r2 < m2.rows; r2++)
        {
            double total = 0.0;
            {
                for (int c = 0; c < m1.cols; c++)
                {
                    total += matrix_get(m1,r1,c) * matrix_get(m2,r2,c);
                }
            }
            matrix_set(m,r1,r2,total);
        }
    }
    return m;
}

matrix_t matrix_multiply_fast(matrix_t m1, matrix_t m2)
{ 
    matrix_t t2 = matrix_transpose(m2);
    matrix_t result = matrix_multiply_transposed(m1,t2);
    //matrix_free(t2);
    return result;
}

void matrix_print(matrix_t m)
{ 
    for (int i = 0; i < m.rows; i++)
    { 
        for (int j = 0; j < m.cols; j++)
        { 
            printf("%g\t", matrix_get(m, i, j));
        }
        printf("\n");
    }
    printf("\n");
}


