# IB Programming in C

This repository is origanised with a folder for each lab, titled `labN` where N is the integral identifier used in the filename of the instructions found [here](https://www.dropbox.com/sh/acp0ztjzoxtnlri/AACO-FTjRXBS4o7wupegwvtka?dl=0). 

The folder `extra-supervision-qs` contains answers to the questions at the end of (last-year's) lecture slides.
