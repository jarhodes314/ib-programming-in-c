#include <stdlib.h>
#include "tree.h"

Tree *empty = NULL;

/* BASE EXERCISE */

int tree_member(int x, Tree *tree)
{
    /*
     * Recurse down the tree until we hit NULL pointer or value == x
     */
    if (!tree) return 0;
    if (x < tree->value) return tree_member(x, tree->left);
    if (x > tree->value) return tree_member(x, tree->right);
    return 1;
}

Tree *tree_insert(int x, Tree *tree)
{ 
    /*
     * If x not member, recurse tree until a NULL is found in correct place
     * Then allocate memory, and set the values, returning a pointer to the tree
     * just created to allocate to parent->left or parent->right from the previous call
     */
    if (tree_member(x, tree)) return tree;
    if (!tree)
    {
        tree = malloc(sizeof(Tree));
        tree->left = NULL;
        tree->value = x;
        tree->right = NULL;
        return tree;
    }
    if (x < tree->value) tree->left = tree_insert(x, tree->left);
    else tree->right = tree_insert(x, tree->right);
    return tree;
}

void tree_free(Tree *tree)
{
    /*
     * First recurse into the subtrees, so that we don't destroy their pointers
     * Then free the current node
     */
    if (!tree) return;
    if (tree->left) tree_free(tree->left);
    if (tree->right) tree_free(tree->right);
    free(tree);
}

/* CHALLENGE EXERCISE */ 

void pop_minimum(Tree *tree, int *min, Tree **new_tree)
{
    /*
     * The minimum is found by going as far left as possible
     * We free the pointer after removing it from the tree
     * new_tree is required in case we delete the head of the tree, which will mean
     *     that the original node will now be released from memory
     */
    if (!tree->left)
    {
        *min = tree->value; /* This copies the original value to the supplied location */
        *new_tree = tree->right;
        free(tree);
        return;
    }
    else
    {
        pop_minimum(tree->left, min, &tree->left);
        new_tree = &tree;
    }
}

Tree *tree_remove(int x, Tree *tree)
{
    /*
     * Traverse the tree until reaching the node with value x
     * If that node has a right subtree, the minimum of that is the value the new node should take
     * Otherwise, simply replace the node with its own left subtree and free the node with value x
     */
    Tree ** new_tree = &tree->right;
    if (!tree) return NULL;
    if (x < tree->value) tree->left = tree_remove(x, tree->left);
    else if (x > tree->value) tree->right = tree_remove(x, tree->right);
    else if (tree->right) pop_minimum(tree->right, &tree->value, new_tree);
    else 
    {
        Tree *node_to_replace = tree;
        if (tree->right)
        {
            pop_minimum(tree->right, &tree->value, &tree->right);
        }
        else {
            tree = tree->left;
            free(node_to_replace);
        }
    }
    return tree;
}
